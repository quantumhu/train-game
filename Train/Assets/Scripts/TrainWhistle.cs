﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainWhistle : MonoBehaviour
{
	
	public AudioClip MusicClip;
	
	public AudioSource MusicSource;
	
	private bool firstRun = true;
	
    // Start is called before the first frame update
    void Start()
    {
        MusicSource.clip = MusicClip;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.K)) {
			if (MusicSource.time > 0.35f || firstRun) {
				MusicSource.Play();
				MusicSource.time = 0.30f;
				firstRun = false;
			}
		} else {
			firstRun = true;
		}
    }
}
