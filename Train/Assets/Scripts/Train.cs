﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Train : MonoBehaviour
{
	
	Rigidbody train;
	Animator anim;
	
	public float speed = 20.0f;
	public float rotateSpeed = 0.1f;
	
	private bool inAir = false;
	
    // Start is called before the first frame update
    void Start()
    {
		train = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
		
		anim.SetBool("isMoving", true);
    }
	
    // Update is called once per frame
    void Update()
    {
		float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
		transform.Rotate(0, moveHorizontal * rotateSpeed, 0);
		Vector3 movementVector = new Vector3(moveHorizontal, 0.0f, moveVertical);
        train.AddForce(movementVector * speed);
		
		anim.SetFloat("animSpeed", train.velocity.magnitude * 0.1f);
		
		CalculateInAir();
		
		if (Input.GetKeyDown(KeyCode.Space)) {
			if (!inAir) {
				train.AddForce(new Vector3(0, 9, 0), ForceMode.Impulse);
				inAir = true;
			}
		}
		
		if (Input.GetKeyDown(KeyCode.Y)) {
			SceneManager.LoadScene("Main");
		}
	
    }
	
	void CalculateInAir() {
		float DistanceToTheGround = GetComponent<Collider>().bounds.extents.y;
 
        bool IsGrounded = Physics.Raycast(transform.position, Vector3.down, DistanceToTheGround + 0.1f);
		
		if (IsGrounded) {
			inAir = false;
		}
	}
}
