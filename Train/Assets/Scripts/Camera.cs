﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
	public GameObject train;
	
	private Vector3 offset;
	private float angleOffset;

	// Start is called before the first frame update
    void Start()
    {
        offset = transform.position - train.transform.position;
		angleOffset = transform.rotation.eulerAngles.y - train.transform.eulerAngles.y;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = train.transform.position + offset;
		//transform.rotation = Quaternion.Euler(0, (train.transform.eulerAngles.y + angleOffset) * 0.1f, 0);
    }
}
